# os-license-checker

WARNING: This is an unofficial project currently in "beta". It _should_ work as expected, but if not, please [open an issue](https://gitlab.com/gitlab-org/os-license-checker/-/issues).

## Overview

The **OS-license-checker** project is a collection of tools designed to help validate whether projects under a given GitLab group namespace have open source licenses and public visibility. It aims to simplify the process of determining whether a GitLab.com group is eligible for the [GitLab Open Source Program](https://about.gitlab.com/solutions/open-source/).

## Prerequistes

- A GitLab.com group namespace with one or more projects
- The `group_id` of the GitLab.com top-level group namespace to scan
- GitLab.com group owner's PAT (Personal Access Token) (with `api` scope)

## How to Use

The tools here include:

- [A GitLab CI pipeline](#gitlab-ci-pipeline)
- [A Docker image that "just works everywhere"](#docker)
- [A standalone shell script](#standalone-shell-script)

### GitLab CI Pipeline

#### For GitLab Team Members

1. Obtain the `group_id` for your top-level group namespace by navigating to `gitlab.com/<group_name>` and copying the Group ID.
2. In the `os-license-checker` project, go to CI/CD > Pipeline. Click the "Run Pipeline" button.
3. Enter the following variable: `GROUP_ID`: `<group_id_copied_earlier>`.
4. View the pipeline log for information and CI job artifacts for results.
5. :tada:

#### For GitLab Community Members

1. Have the owner of a group fork this project into their personal namespace.
2. Have the group owner generate a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` scope.
3. Set a the group owner PAT as protected, masked variable in the forked project: `GLPAT`: `<group_owner_pat>`.
4. Obtain the `group_id` for your top-level group namespace by navigating to `gitlab.com/<group_name>` and copying the Group ID.
5. In the `os-license-checker` project, go to CI/CD > Pipeline. Click the "Run Pipeline" button.
6. Enter the following variable: `GROUP_ID`: `<group_id_copied_earlier>`.
7. View the pipeline log for information and CI job artifacts for results.
8. :tada:

### Docker

OS-license-checker can also be executed using Docker.

To use the Docker image, run the following command from the directory where you want the report to be saved:

```sh
docker run -it -v "$(pwd)":/tmp registry.gitlab.com/gitlab-org/os-license-checker:cli
```

You will be asked to provide the following:

- `GROUP_ID`: The GitLab top-level Group ID you want to check
- `GLPAT`: GitLab Group Owner's Personal Access Token with `api` scope

Once the script finishes, a `$GROUP_NAME-os-license-scanner.csv` file will be created in the directory where the command was run.

### Standalone Shell Script

OS-license-checker can also be executed as a standalone shell script.

Requirements:
- [jq](https://stedolan.github.io/jq/)
- [glab](https://gitlab.com/gitlab-org/cli/#installation)

The `os-license-checker.sh` script can be reviewed and downloaded here: https://gitlab.com/gitlab-org/os-license-checker/-/raw/main/license-checker.sh

Or to run it directly from the command line:

```sh
curl https://gitlab.com/gitlab-org/os-license-checker/-/raw/master/license-checker.sh | bash
```

You will be asked to provide the following:

- `GROUP_ID`: The GitLab top-levelGroup ID you want to check
- `GLPAT`: GitLab Group Owner's Personal Access Token with `api` scope

Once the script completes, you will find a `$GROUP_NAME-os-license-scanner.csv` file in the directory where the command was run.

### Contributing

This is an open source project, and we welcome your contributions!

Please feel free to open a merge request or issue.
