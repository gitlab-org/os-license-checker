FROM ubuntu:22.04

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get upgrade -y && \
    apt-get install --no-install-recommends -y -q ca-certificates curl jq && \
    rm -rf /var/lib/apt/lists/* /var/cache/debconf/templates.* /var/lib/dpkg/status-old

RUN curl -L -s https://gitlab.com/greg/scripts/-/raw/master/glab-install.sh | bash && \
    curl -L -s https://gitlab.com/greg/scripts/-/raw/master/glab-update-deb.sh | bash
