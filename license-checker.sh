#!/usr/bin/env bash
# This script checks for open source licenses and project visibility.

# Exit immediately if a command exits with a non-zero status.
set -euo pipefail

# Function to check if a command is available
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if necessary commands are available
for cmd in glab jq; do
  if ! command_exists "$cmd"; then
    printf "%s is not installed. Please install it first.\n" "$cmd"
    exit 1
  fi
done

# Function to prompt for GROUP_ID if empty
get_group_id() {
  if [[ -z "${GROUP_ID:-}" ]]; then
    printf "\033[1mPlease enter the top-level GitLab group ID to scan\033[0m\n"
    read -r -p "Group ID: " GROUP_ID
  fi

  # Check if the variable GROUP_ID is valid
  if [[ ! "$GROUP_ID" =~ ^[1-9][0-9]{0,11}$ ]]; then
    printf "\033[1mERROR:\033[0m Group ID must be a number\n"
    exit 1
  fi
}

# Function to prompt for GLPAT if empty
get_glpat() {
  if [[ -z "${GLPAT:-}" ]]; then
    printf "\033[1mPlease enter the GitLab Personal Access Token (PAT) of the Group Owner\033[0m\n"
    read -r -p "Group Owner PAT: " GLPAT
  fi

  # Check if the variable GLPAT is valid
  if [[ ! "$GLPAT" =~ ^glpat-[0-9a-zA-Z_-]{15,25}$ ]]; then
    printf "\033[1mERROR:\033[0m GitLab PAT must be in the format \033[1mglpat-xxxxxxxxxxxxxxxxxxxx\033[0m\n"
    exit 1
  else
    clear -x
  fi
}

get_group_id
get_glpat

# Log in to GitLab
glab auth login -t "$GLPAT"

# Get the group namespace
GROUP_NAMESPACE=$(glab api "groups/$GROUP_ID" | jq -r '.path')
GROUP_PATH=$(glab api "groups/$GROUP_ID" | jq -r '.full_path')

echo -e "Checking projects in \033[1m$GROUP_PATH\033[0m for open source licenses and project visibility..."

# Create temporary files for project IDs and report
project_ids=$(mktemp)
report=$(mktemp)

# Get the project IDs and store them in a file
glab api --paginate "groups/$GROUP_ID/projects?include_subgroups=true&per_page=100" | jq -r '.[].id' > "$project_ids"

# Get the project details and store them in a report
while read -r id; do 
    glab api "projects/$id?license=true" | jq -r '[.id, .path, .web_url, .visibility, if .license.key == null then "none" else .license.name end] | @csv' >> "$report"
done < "$project_ids"

# Count the projects with non-FOSS licenses and without a license
echo -e "There are \033[1m$(grep -c Other "$report") project(s)\033[0m with a non-(F)OSS license in this group namespace"
echo -e "There are \033[1m$(grep -c none "$report") project(s)\033[0m without a license in this group namespace"

# Count the private projects
echo -e "There are \033[1m$(grep -c private "$report") private project(s)\033[0m in this group namespace"

# Add a header to the final report and append the project details
printf '"Project ID","Project Name","Project URL","Visibility","License"\n' > "/tmp/$GROUP_NAMESPACE-os-license-checker-report.csv"

# Filter the report by the group namespace and append the project details
grep "$GROUP_NAMESPACE" "$report" >> "/tmp/$GROUP_NAMESPACE-os-license-checker-report.csv"

# Trap EXIT signal to ensure the temporary files are removed
trap 'rm -f "$project_ids" "$report"' EXIT

echo -e "Report saved to \033[1m$GROUP_NAMESPACE-os-license-checker-report.csv\033[0m"
